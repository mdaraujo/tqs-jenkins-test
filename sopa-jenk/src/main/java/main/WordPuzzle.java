/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;

/**
 *
 * @author Miguel
 */
public class WordPuzzle {

    private char[][] puzzle;
    private List<String> words;

    public WordPuzzle(char[][] puzzle, List<String> words) {
        this.puzzle = puzzle.clone();
        this.words = words;
    }

    //Generate
    public WordPuzzle(List<String> words) {

        this.words = new ArrayList(words);
        int numChars = sortWords(words);

        int size = (int) Math.sqrt(numChars) + 3;

        if (size < words.get(0).length() + 3) {
            size = words.get(0).length() + 3;
        }

        if (size < words.size() + 3) {
            size = words.size() + 3;
        }

        this.puzzle = new char[size][size];
        Random random = new Random();

        for (int i = 0; i < words.size(); i++) {

            boolean fitted = false;
            String word = words.get(i);

            do {
                int l = random.nextInt(size);
                int c = random.nextInt(size);

                if (puzzle[l][c] == 0) {

                    fitted = fitWord(l, c, word);
                }

            } while (!fitted);

        }

        for (int l = 0; l < size; l++) {
            for (int c = 0; c < size; c++) {
                if (puzzle[l][c] == 0) {
                    puzzle[l][c] = (char) (random.nextInt(26) + 'A');
                }
            }
        }
    }

    private boolean fitWord(int l, int c, String word) {
        List<Direction> directions = Arrays.asList(Direction.values());

        Collections.shuffle(directions);

        for (Direction direction : directions) {
            if (isWordFitInDirection(l, c, word, direction) && fitWordInDirection(l, c, word, direction)) {
                return true;
            }
        }
        return false;
    }

    //Generate
    private boolean fitWordInDirection(int l, int c, String word, Direction direction) {
        for (int i = 0; i < word.length(); i++) {
            if (puzzle[l + (direction.getY() * i)][c + (direction.getX() * i)] != 0) {
                return false;
            }
        }
        for (int i = 0; i < word.length(); i++) {
            puzzle[l + (direction.getY() * i)][c + (direction.getX() * i)] = word.charAt(i);
        }
        return true;
    }
    
    //Sort words and return the number of all chars
    public static int sortWords(List<String> arr) {

        int sum = 0;

        for (int i = 0; i < arr.size() - 1; i++) {

            int index = i;
            for (int j = i + 1; j < arr.size(); j++) {
                if (arr.get(j).length() > arr.get(index).length()) {
                    index = j;
                }
            }
            String aux = arr.get(index);
            arr.set(index, arr.get(i));
            arr.set(i, aux);

            sum += arr.get(i).length();
        }
        return sum;
    }

    //Solve
    public Result[] solve() {
        List<String> searchList = new ArrayList(words);
        Result[] results = new Result[words.size()];

        for (int l = 0; l < puzzle.length; l++) {
            for (int c = 0; c < puzzle.length; c++) {
                Result result = checkLetter(l, c);
                if (result != null) {
                    results[words.indexOf(result.getWord())] = result;
                    searchList.remove(result.getWord());
                }
            }
        }
        return results;
    }

    //Solve
    private Result checkLetter(int l, int c) {
        for (int i = 0; i < words.size(); i++) {
            if (words.get(i).charAt(0) == puzzle[l][c]) {
                String word = words.get(i);
                Result result = searchWord(l, c, word);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    //Solve
    private Result searchWord(int l, int c, String word) {
        List<Direction> directions = Arrays.asList(Direction.values());
        for (int j = 0; j < directions.size(); j++) {
            if (isWordFitInDirection(l, c, word, directions.get(j))) {
                Result result = searchWordInDirection(l, c, word, directions.get(j));
                if (result != null) {
                    return result;
                }
            }

        }
        return null;
    }

    //Solve
    private boolean isWordFitInDirection(int l, int c, String word, Direction direction) {

        switch (direction) {
            case RIGHT:
                if (word.length() <= puzzle.length - c) {
                    return true;
                }
                break;

            case LEFT:
                if (word.length() <= c + 1) {
                    return true;
                }
                break;

            case UP:
                if (word.length() <= l + 1) {
                    return true;
                }
                break;

            case DOWN:
                if (word.length() <= puzzle.length - l) {
                    return true;
                }
                break;

            case DOWNRIGHT:
                if (word.length() <= puzzle.length - l && word.length() <= puzzle.length - c) {
                    return true;
                }
                break;

            case DOWNLEFT:
                if (word.length() <= puzzle.length - l && word.length() <= c + 1) {
                    return true;
                }
                break;

            case UPRIGHT:
                if (word.length() <= l + 1 && word.length() <= puzzle.length - c) {
                    return true;
                }
                break;

            case UPLEFT:
                if (word.length() <= l + 1 && word.length() <= c + 1) {
                    return true;
                }
                break;

            default:
                return false;
        }
        return false;
    }

    //Solve
    private Result searchWordInDirection(int l, int c, String word, Direction direction) {
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) != puzzle[l + (direction.getY() * i)][c + (direction.getX() * i)]) {
                return null;
            }
        }
        return new Result(word, l, c, direction);
    }

    //Verifica se a lista de palavras contem palavras duplicadas ou frases redundantes
    public boolean duplicatedWords() {
        for (int i = 0; i < words.size() - 1; i++) {
            for (int j = i + 1; j < words.size(); j++) {
                if ((words.get(i).contains(words.get(j))) || (words.get(j).contains(words.get(i)))) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();

        for (int l = 0; l < puzzle.length; l++) {
            s.append(puzzle[l]);
            s.append("\n");
        }

        for (int i = 0; i < words.size(); i++) {
            s.append(words.get(i).toLowerCase(Locale.ENGLISH));

            if (i != words.size() - 1) {
                s.append(", ");
            }

        }

        return s.toString();
    }

    public char[][] getPuzzle() {
        return puzzle.clone();
    }

    public void setPuzzle(char[][] puzzle) {
        this.puzzle = puzzle.clone();
    }

    public List<String> getWords() {
        return words;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }

}
