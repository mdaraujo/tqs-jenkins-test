/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import util.PuzzleException;

/**
 *
 * @author Miguel
 */
public class Main {

    private static final String PATH_TO_FILES = "src/main/java/files/";
    private static final String CHARSET = "UTF-8";

    private Main() {
    }

    public static void main(String[] args) {

        String inputFile = "palavras.txt";
        String outputFile = "nova_sopa.txt";

        List<String> words = readWordsFromFile(inputFile);
        WordPuzzle generatedPuzzle = new WordPuzzle(words);
        printInFile(outputFile, generatedPuzzle.toString());
        System.out.println(generatedPuzzle);
        System.out.println();

        long startTime = System.currentTimeMillis();

        WordPuzzle wordPuzzle = readPuzzleFromFile(outputFile);

        if (wordPuzzle.duplicatedWords()) {
            throw new PuzzleException("A lista de palavras não pode conter palavras duplicadas ou frases redundantes.");
        }

        Result[] results = wordPuzzle.solve();

        long milliseconds = System.currentTimeMillis() - startTime;
        float seconds = milliseconds / 1000f;

        System.out.printf("Puzzle resolvido em %.3f segundos.%n", seconds);
        System.out.println();
        System.out.printf("%-10s %-6s %-6s %-10s %n", "PALAVRA", "LINHA", "COLUNA", "DIREÇÃO");

        for (Result result : results) {
            if (result == null) {
                throw new PuzzleException("Todas as palavras da lista têm de estar no puzzle.");
            }
            System.out.println(result);
        }
    }

    private static void printInFile(String fileName, String puzzle) {

        File file = new File("src/main/java/files/" + fileName);

        try (Writer w = new OutputStreamWriter(new FileOutputStream(file), "UTF-8")) {
            try (PrintWriter printWriter = new PrintWriter(w)) {
                printWriter.println(puzzle);
            }
        } catch (IOException e) {
            throw new PuzzleException(e.getMessage(), e);
        }
    }

    private static List<String> readWordsFromFile(String fileName) {

        List<String> words = new ArrayList();
        String line;
        File file = new File(PATH_TO_FILES, fileName);

        try (Scanner sc = new Scanner(file, CHARSET)) {
            while (sc.hasNextLine()) {
                line = sc.nextLine();
                words.addAll(processLine(line));
            }

        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new PuzzleException(e.getMessage(), e);
        }

        return words;
    }

    private static WordPuzzle readPuzzleFromFile(String fileName) {

        char[][] puzzle;
        List<String> words = new ArrayList();
        String line;
        File file = new File(PATH_TO_FILES, fileName);

        try (Scanner sc = new Scanner(file, CHARSET)) {

            line = sc.nextLine();
            int size = line.length();

            if (size > 80) {
                throw new PuzzleException("O tamanho máximo do puzzle é 80*80.");
            }
            puzzle = new char[size][size];
            puzzle[0] = line.toCharArray();
            int puzzleLine = 1;

            while (sc.hasNextLine()) {
                line = sc.nextLine();
                if (puzzleLine < size) {
                    validatePuzzleLine(line, size);
                    puzzle[puzzleLine] = line.toCharArray();
                    puzzleLine++;
                } else {
                    words.addAll(processLine(line));
                }
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new PuzzleException(e.getMessage(), e);
        }

        return new WordPuzzle(puzzle, words);
    }

    public static List<String> processLine(String line) {
        if (line.isEmpty()) {
            throw new PuzzleException("A lista de palavras não podem conter linhas em branco.");
        }
        if (line.contains(",")) {
            return getWordsFromLine(line, ",");
        } else if (line.contains(";")) {
            return getWordsFromLine(line, ";");
        } else if (line.contains(" ")) {
            return getWordsFromLine(line, " ");
        } else {
            return getWordsFromLine(line, "");
        }
    }

    public static List<String> getWordsFromLine(String line, String charToSplit) {
        if ("".equals(charToSplit)) {
            String word = line.trim();
            validateWord(word);
            return Arrays.asList(word.toUpperCase(Locale.ENGLISH));
        }
        List<String> words = new ArrayList();
        String[] split = line.split(charToSplit);
        for (String s : split) {
            s = s.trim();
            validateWord(s);
            words.add(s.toUpperCase(Locale.ENGLISH));
        }
        return words;
    }

    public static void validateWord(String word) {
        if (word.length() < 2) {
            throw new PuzzleException("As palavras têm de ter pelo menos dois caractéres.");
        }
        for (char letter : word.toCharArray()) {
            if (!Character.isAlphabetic(letter)) {
                throw new PuzzleException("As palavras são compostas apenas por caractéres alfabéticos.");
            }
        }
    }

    public static void validatePuzzleLine(String line, int size) {
        if (line.length() != size) {
            throw new PuzzleException("O puzzle tem de ser quadrado");
        }
        for (char letter : line.toCharArray()) {
            if (Character.isLowerCase(letter)) {
                throw new PuzzleException("As letras do puzzle têm de estar em maiúscula.");
            }
            if (!Character.isAlphabetic(letter)) {
                throw new PuzzleException("O puzzle apenas pode conter letras.");
            }
        }
    }
}
