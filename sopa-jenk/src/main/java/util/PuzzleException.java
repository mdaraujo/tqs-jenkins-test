package util;

public class PuzzleException extends RuntimeException {

    public PuzzleException(String errorMessage) {
        super(errorMessage);
    }

    public PuzzleException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
